import telebot
import datetime
import time
import pika

from telebot import types


TOKEN = '278871738:AAGtxLBSD9SNjisK3g48Hq0SEegtmk78lww' 

bot = telebot.TeleBot(TOKEN)

markup = types.ReplyKeyboardMarkup()

anterior = types.KeyboardButton('anterior')
siguiente = types.KeyboardButton('siguiente')

bajarvolumen = types.KeyboardButton('bajarvolumen')
subirvolumen = types.KeyboardButton('subirvolumen')

pausar = types.KeyboardButton('pausar')

markup.row(anterior,siguiente)
markup.row(bajarvolumen, pausar, subirvolumen)

connection = pika.BlockingConnection(pika.URLParameters('amqp://ftocjkoj:k37PkDR_JQhcVH9A4uvejewcZtMEiSSB@wildboar.rmq.cloudamqp.com/ftocjkoj'))
channel = connection.channel()

channel.queue_declare(queue='hello')


@bot.message_handler(commands=['start'])
def inicio(message):
    bot.reply_to(message, "Hola, soy tu parlante SAIKO, que puedo hacer por vos?", reply_markup=markup)

@bot.message_handler(commands=['help'])
def comandos(message):
    bot.reply_to(message, "Acciones: anterior, siguiente, pausar, bajarvolumen, subirvolumen")

@bot.message_handler(func=lambda m: True)
def escuchar(message):
	ts = time.time()
	st = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S$%Y/%m/%d')
	channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=message.text+"#"+st)

bot.polling()
